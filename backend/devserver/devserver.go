package main

import (
	"net/http"

	"bitbucket.org/wolfgang_meyers/serverless-web-template/backend/handler"
)

func main() {
	r := handler.GetRouter()

	http.ListenAndServe(":5000", r)
}
