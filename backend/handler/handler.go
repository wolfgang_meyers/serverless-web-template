package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// GetRouter returns a router that handles http requests
// for the TODO application
func GetRouter() *gin.Engine {
	r := gin.New()
	r.GET("/", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "Hello, World!")
	})
	return r
}
