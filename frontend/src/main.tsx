import * as React from "react";
import * as ReactDOM from "react-dom";

import { App } from "./components/App";

import "./assets/css/bootstrap.min.css";
import "./assets/css/main.css";

ReactDOM.render(
    <App />,
    document.getElementById("app")
);