# README #

This project provides a template for creating serverless web applications. It is
implemented as a simple TODO web application, and is powered with
the following technologies:

* Golang + Lambda + API Gateway backend
* React + Bootstrap + Typescript + S3 frontend
* Google oauth authentication
* DynamoDB persistence layer
* Bitbucket Pipelines Build and Deployment

### What is this repository for? ###

I wanted to build this web app as a starting point for future serverless web applications.

### How do I get set up? ###

TBD